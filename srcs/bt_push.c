
#include <stdlib.h>
#include <string.h>

#include "test.h"

void		bt_push(backtrace bt, const char *msg)
{
	size_t		i;

	if (bt)
	{
		pthread_mutex_lock(&bt->lock);
		for (i = 0; bt->backtrace[i][0] != '\0'; i++) {}

		strcpy(bt->backtrace[i], msg);
		pthread_mutex_unlock(&bt->lock);
	}
}
