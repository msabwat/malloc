

#include <stdlib.h>
#include <unistd.h>
#include "test.h"

void	bt_print(backtrace bt)
{
	size_t		i;

	if (bt)
	{
		pthread_mutex_lock(&bt->lock);
		for (i = 0; bt->backtrace[i][0] != '\0'; i++) {
			ft_dprintf(STDERR_FILENO, "-> [%s] ", bt->backtrace[i]);
		}
		pthread_mutex_unlock(&bt->lock);
	}
}
