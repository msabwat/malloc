#include <string.h>
#include "test.h"

backtrace	bt_initialize(void)
{
	backtrace bt = malloc(sizeof(*bt));

	if (bt != NULL)
	{
		memset(bt->backtrace, 0, sizeof(bt->backtrace));
		pthread_mutex_init(&bt->lock, NULL);
	}
	return bt;
}
